#!/bin/sh

# Export the GPIOs defined by the Q7 standard
#
# the GPIOs 0 ... 7 are currently mapped as inputs
# in order to define them as output, uncomment the "echo "out" ... direction" statement underneath the related pin export statement


# Pin mapping
#    Q7        |                     | Linux   |
#  PIN | Name  | alt. Function       |   GPIO  | Device-Tree Pad                   | Remarks
# -----|-------|---------------------|---------|-----------------------------------|---------
#  185 | GPIO0 | hdmi_tx CEC_LINE    | gpio130 | MX6QDL_PAD_EIM_A25__GPIO5_IO02    |
#  186 | GPIO1 | USBOTG_PWR          | gpio86  | MX6QDL_PAD_EIM_D22__GPIO3_IO22    | Rev. B.x
#  187 | GPIO2 | CSPI3 CS2#          | gpio122 | MX6QDL_PAD_DISP0_DAT5__GPIO4_IO26 |
#  188 | GPIO3 | CSPI3 CS3#          | gpio123 | MX6QDL_PAD_DISP0_DAT6__GPIO4_IO27 |
#  189 | GPO1  | CLKO Audio Ref Clk  | gpio0   | MX6QDL_PAD_GPIO_0__GPIO1_IO00     |
#  190 | GPI   | CAN2 RXCAN          | gpio111 | MX6QDL_PAD_KEY_ROW4__GPIO4_IO15   |
#  191 | GPIO4 | ENET_1588_EVENT2_IN | gpio203 | MX6QDL_PAD_GPIO_16__GPIO7_IO11    |
#  192 | GPO0  | CAN2 TXCAN          | gpio110 | MX6QDL_PAD_KEY_COL4__GPIO4_IO14   |

# for rev c.x GPIO1 has changed:
#      | GPIO1 |                     | gpio8   | MX6QDL_PAD_GPIO_8__GPIO1_IO08     | Rev. C.x

# Remark:
# for iMX6: linux gpio number = (gpio_bank - 1) * 32 + gpio_bit
# eg: MX6QDL_PAD_EIM_A25__GPIO5_IO02 = (5 - 1) * 32 + 2 = 130



do_start() {
	# Q7, Pin 185, GPIO 0
	echo 130 > /sys/class/gpio/export
	#echo "out" > /sys/class/gpio/gpio130/direction

	# Q7, Pin 186, GPIO 1
	# disabled, because this pin is shared with OTG_PWR functionality
	# in order to enable the pin as GPIO, the kernel's device tree configuration has to be changed
	#echo 86 > /sys/class/gpio/export
	#echo "out" > /sys/class/gpio/gpio86/direction

	# Q7, Pin 187, GPIO 2
	echo 122 > /sys/class/gpio/export
	#echo "out" > /sys/class/gpio/gpio122/direction

	# Q7, Pin 188, GPIO 3
	echo 123 > /sys/class/gpio/export
	#echo "out" > /sys/class/gpio/gpio123/direction

	# Q7, Pin 189, GPIO 4
	echo 0 > /sys/class/gpio/export
	#echo "out" > /sys/class/gpio/gpio0/direction

	# Q7, Pin 190, GPIO 5
	# Attention: could lead to a conflict when specified as output and used along with a backplane which has a LPC device assembled
	echo 111 > /sys/class/gpio/export
	#echo "out" > /sys/class/gpio/gpio111/direction

	# Q7, Pin 191, GPIO 6
	# Attention: could lead to a conflict when specified as output and used along with a backplane which has a LPC device assembled
	echo 203 > /sys/class/gpio/export
	#echo "out" > /sys/class/gpio/gpio203/direction

	# Q7, Pin 192, GPIO 7
	# Attention: could lead to a conflict when specified as output and used along with a backplane which has a LPC device assembled
	echo 110 > /sys/class/gpio/export
	#echo "out" > /sys/class/gpio/gpio110/direction

}

do_stop() {
	echo 110 > /sys/class/gpio/unexport
	echo 203 > /sys/class/gpio/unexport
	echo 111 > /sys/class/gpio/unexport
	echo 0 > /sys/class/gpio/unexport
	echo 123 > /sys/class/gpio/unexport
	echo 122 > /sys/class/gpio/unexport
	#echo 86 > /sys/class/gpio/unexport
	echo 130 > /sys/class/gpio/unexport
}

case "$1" in
	start)
	do_start
		;;
	restart|reload|force-reload)
		echo "Error: argument '$1' not supported" >&2
		exit 3
		;;
	stop)
	do_stop
		;;
	*)
		echo "Usage: $0 start|stop" >&2
		exit 3
		;;
esac

