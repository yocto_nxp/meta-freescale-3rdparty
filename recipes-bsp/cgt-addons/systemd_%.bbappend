# Enable poweroff button handling with systemd

COMPATIBLE_MACHINE = "cgtqmx6"

do_install_append() {

    # poweroffbutton handling with systemd is disabled in
    #   <BSP-dir>/meta-imx/meta-bsp/recipes-core/systemd/systemd_%.bbappend
    #   with patch 0020-logind.conf-Set-HandlePowerKey-to-ignore.patch
    #
    # re-enable poweroff key handling with systemd
    sed -i 's/^HandlePowerKey=ignore$/HandlePowerKey=poweroff/g' ${D}${sysconfdir}/systemd/logind.conf

    # also enable suspend key
    sed -i 's/^#HandleSuspendKey=suspend$/HandleSuspendKey=suspend/g' ${D}${sysconfdir}/systemd/logind.conf

    # also enable lid switch
    sed -i 's/^#HandleLidSwitch=suspend$/HandleLidSwitch=suspend/g' ${D}${sysconfdir}/systemd/logind.conf
}
