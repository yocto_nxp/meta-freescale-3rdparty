# Enable Congatec QMX6 Q7 GPIO's

LICENSE = "GPL-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"


SRC_URI = " file://cgt-qmx6-gpios.sh"
SRC_URI += "file://cgt-qmx6-gpios.service"


inherit systemd
REQUIRED_DISTRO_FEATURES= "systemd"
SYSTEMD_SERVICE_${PN} = "cgt-qmx6-gpios.service"
SYSTEMD_AUTO_ENABLE_${PN} = "enable"

do_install(){
	install -d ${D}${base_sbindir}
	install -m 0755 ${WORKDIR}/cgt-qmx6-gpios.sh ${D}${base_sbindir}

	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/cgt-qmx6-gpios.service ${D}${systemd_unitdir}/system
}

PACKAGE_ARCH = "${MACHINE_ARCH}"
COMPATIBLE_MACHINE = "cgtqmx6"
