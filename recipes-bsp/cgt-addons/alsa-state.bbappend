# Set first audio card to default output for asound

COMPATIBLE_MACHINE = "cgtqmx6"

do_install_append() {

    sed -i '1s/^/defaults.pcm.card 0\ndefaults.ctl.card 0\n\n/' ${D}${sysconfdir}/asound.conf
}
