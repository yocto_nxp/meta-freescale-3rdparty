# Congatec example to handle the buttons on the backplane

LICENSE = "GPL-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"


SRCREV = "8a0fdc959e43498692dbc3cc2c7d373ff2401778"
SRC_URI = "git://git.congatec.com/arm/qmx6_addons.git;protocol=https;branch=master \
	   "
S = "${WORKDIR}/git"

do_compile(){
	cd btevent
	make
}

do_install() {
	install -d ${D}/usr/bin
	install -m 0777 ${S}/btevent/btevent ${D}/usr/bin
}

do_deploy () {
    install -d ${DEPLOYDIR}
    install -0777 ${S}/btevent/btevent ${DEPLOYDIR}/usr/bin
}

FILES_${PN} = "\
		/usr/bin/btevent \
"

PACKAGE_ARCH = "${MACHINE_ARCH}"
COMPATIBLE_MACHINE = "cgtqmx6"
