# Congatec QMX6 Linux/kernel

require recipes-kernel/linux/linux-imx.inc

# inherit from "kernel-yocto"
#  - this is a workaround in zeus (yocto 3.0) to use the intree defconfig handling with KBUILD_DEFCONFIG
#  - this is not needed in dunfell (yocto 3.1) because linux-imx already inherits it there
require recipes-kernel/linux/linux-yocto.inc
inherit kernel-yocto


SUMMARY = "Linux Kernel for Congatec QMX6 boards"

LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"


SRC_URI = "git://git.congatec.com/arm/imx6_kernel_5.4.git;protocol=https;branch=${SRCBRANCH}"
SRCREV = "4431dd2dd73c8dfc1ea1bda2c78c0bd0819b8c2f"

SRCBRANCH = "cgtimx6__imx_5.4.3_1.0.0"
DEPENDS += "lzop-native bc-native"

COMPATIBLE_MACHINE = "cgtqmx6"
LOCALVERSION = "-cgtqmx6"


# Tell to kernel class that we would like to use our defconfig to configure the kernel.
# Otherwise, the --allnoconfig would be used per default which leads to mis-configured
# kernel.
#
# This behavior happens when a defconfig is provided, the kernel-yocto configuration
# uses the filename as a trigger to use a 'allnoconfig' baseline before merging
# the defconfig into the build.
#
# If the defconfig file was created with make_savedefconfig, not all options are
# specified, and should be restored with their defaults, not set to 'n'.
# To properly expand a defconfig like this, we need to specify: KCONFIG_MODE="--alldefconfig"
# in the kernel recipe include.
#
# workaround in zeus (yocto 3.0) - in dunfell it is set in linux-imx
KCONFIG_MODE="--alldefconfig"



# Set the Kernel defconfig file to the intree qmx6_defconfig file.
# To use a custom kernel defconfig:
#   - copy that custom defconfig to eg. "linux-congatec/defconfig"
#   - create a bbappend file "linux-congatec_%.bbappend" and add
#       FILESEXTRAPATHS_prepend := "${THISDIR}/linux-congatec:"
#       SRC_URI += "file://defconfig"
S = "${WORKDIR}/git"
KBUILD_DEFCONFIG_pn-linux-congatec = "qmx6_defconfig"


